#!/bin/bash
# Author:  Tealk

SNOWPATH="/home/snowflake"
FOLDER="/proxy"
FULLPATH="$SNOWPATH/$FOLDER"

git -C "${SNOWPATH}" fetch --all >/dev/null 2>&1
uptodate=$(git -C "${SNOWPATH}" log HEAD..origin/master --oneline)
if [[ ${uptodate} = "" ]]; then
  echo "Repo is up to date"
  exit 0 
else
  kill -9 "$(pidof proxy)"
  git -C "${SNOWPATH}" pull origin master >/dev/null 2>&1
  cd ${FULLPATH} || exit
  if ! go build; then
    echo "build failed - exit"
    exit 1
  else
    nohup ${FULLPATH}/proxy > ${FULLPATH}/snowflake.log 2>&1 &
    exit 0 
  fi
fi
